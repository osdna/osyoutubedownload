# osyoutubedownload



The youtube download [shortcut](https://support.apple.com/fr-fr/guide/shortcuts/welcome/ios) allows you to download YouTube videos in a few clicks on iOS.

To work properly, the extension uses the following resources in particular :

- The application [a-Shell](https://holzschu.github.io/a-Shell_iOS/)
- The command line program [youtube-dl](https://ytdl-org.github.io/youtube-dl/)


The extension works perfectly with the YouTube website, and also supports other sites.
For a complete list of supported sites, please refer to the link below :

- [https://ytdl-org.github.io/youtube-dl/supportedsites.html](https://ytdl-org.github.io/youtube-dl/supportedsites.html)



 

## Requirements

As mentioned before, to work properly the extension needs the following elements :


* [a-Shell](https://holzschu.github.io/a-Shell_iOS/)
* [youtube-dl](https://ytdl-org.github.io/youtube-dl/)



 

## Standard installation


Please follow the procedure below in order to use the extension :


```text

1. First, download the a-Shell application available on the iOS App Store.

```

*  a-Shell [https://holzschu.github.io/a-Shell_iOS/](https://holzschu.github.io/a-Shell_iOS/)

*  <a href="https://gitlab.com/osxcode/osyoutubedownload/-/blob/main/public/shell.mp4">Video of the application download.</a>

--



```text

2. Secondly, enter the following command line in the a-Shell application and press the enter key.

This command line allows the installation of the command line program command line program youtube-dl.

```

```bash

pip install youtube_dl

```

*  <a href="https://gitlab.com/osxcode/osyoutubedownload/-/blob/main/public/pip.mp4">Video of the execution of the command line.</a>

--



```text

3. Download and go to the shortcut application available on the App Store. 																	
   Once downloaded, open the shortcut application and click on "+".

   You should then get the following window on the shortcut application :

```

--


```text

4. Once the previous step has been completed correctly, the user can add the various actions necessary for the behavior of the shortcut.

Here is the list of the different actions to add in chronological order :


    4.1 Add the action "request to enter" with :
    
        - Type of input : URL 
        - Prompt : YouTube URL :


    4.2 Add the action "define a variable" with :
    
        - Name of the variable : URL
        - Input :  Default URL


     4.3 Add a dictionary with :

        - Key : SHELL
        - Value : python -m youtube_dl -f best "*URL"

        *URL : Corresponds to the variable of the URL


    4.4 Add the action "Define a variable" with :

        - Name of the variable : YDOW
        - Input : The value of the SHELL key in the dictionary
        
        
    4.5 Add the "Open app" action with :

        - Choose : a-Shell
        

    4.6 Add the "Wait" action with:

        - 2 second
        
        
    4.7 Add the "Apps" action defined in a-Shell with the following commands :

        - clear
        - cd ~/Documents/
        - *YDOW

        *YDOW : Corresponds to the variable YDOW
        
    
    4.8 Access to the shortcut to the a-Shell application.

```


```text

You should then obtain the following result :

```

*  <a href="https://gitlab.com/osxcode/osyoutubedownload/-/blob/main/public/result.mp4">Video result.</a>




## Use


Once the previous steps are completed the user can run the newly created extension and provide a YouTube URL.

Here is an example of how the shortcut works :


*  <a href="https://gitlab.com/osxcode/osyoutubedownload/-/blob/main/public/use.mp4">Video of using the shortcut.</a>






## Tests



#### Hardware / Software



<span style="color:red">Information - The tests as well as the development of the shortcut have been carried out with the versions of the following tools and systems : </span>


- [System](https://en.wikipedia.org/wiki/IOS_14) : iOS 14.4

- [Hardware](https://en.wikipedia.org/wiki/IPhone_7) : iPhone 7 Plus

- [a-Shell](https://holzschu.github.io/a-Shell_iOS/) - 1.6.9

- [youtube-dl](https://ytdl-org.github.io/youtube-dl/) - 2021.3.14



 

## Maintainers


Current maintainer :

* osxcode - [https://gitlab.com/osxcode/osyoutubedownload](https://gitlab.com/osxcode/osyoutubedownload)



 

## Privacy

<span style="color:red">The extension does not collect or store any personal data about the user. </span>

When downloading the video, https connection informations between the user and the server may be transmitted.




## Warning

It is the end user's responsibility to obey all applicable local, state and federal laws.

The author is in no way responsible for any misuse or damage caused by this program.



 

## Support

If you want you can support me ;)

[-- OXEN --](https://oxen.io)


<img src="https://osxcode.gitlab.io/os/img/QRCODE_OXEN_WALLET.png"  width="500" height="350">



 

## Licence  

[LGPL - Lesser General Public License](https://www.gnu.org/licenses/lgpl-3.0.html)

